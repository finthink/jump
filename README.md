# Is the Jump-Diffusion Model a Good Solution for Credit Risk Modeling? The Case of Convertible Bonds

A company can raise capital in financial markets either by issuing equities, bonds, or hybrids (such as convertible bonds). From an investor’s perspective, convertible bonds with embedded optionality offer certain benefits of both equities and bonds – like the former, they have the potential for capital appreciation and like the latter, they offer interest income and safety of principal. The convertible bond market is of primary global importance. 

There is a rich literature on the subject of convertible bonds. Arguably, the first widely adopted model among practitioners is the one presented by Goldman Sachs (1994) and then formalized by Tsiveriotis and Fernandes (1998). The Goldman Sachs’ solution is a simple one factor model with an equity binomial tree to value convertible bonds. The model considers the probability of conversion at every node. If the convertible is certain to remain a bond, it is then discounted by a risky discount rate that reflects the credit risk of the issuer. If the convertible is certain to be converted, it is then discounted by the risk-free interest rate that is equivalent to default free.

Tsiveriotis and Fernandes (1998) argue that in practice one is usually uncertain as to whether the bond will be converted, and thus propose dividing convertible bonds into two components: a bond part that is subject to credit risk and an equity part that is free of credit risk. A simple description of this model and an easy numerical example in the context of a binomial tree can be found in Hull (2003).

Grimwood and Hodges (2002) indicate that the Goldman Sachs model is incoherent because it assumes that bonds are susceptible to credit risk but equities are not. Ayache, et al. (2003) conclude that the Tsiveriotis-Fernandes model is inherently unsatisfactory due to its unrealistic assumption of stock prices being unaffected by bankruptcy. To correct this weakness, Davis and Lischka (1999), Andersen and Buffum (2004), Bloomberg (2009), and Carr and Linetsky (2006) etc., propose a jump-diffusion model to explore defaultable stock price dynamics. They all believe that under a risk-neutral measure the expected rate of return on a defaultable stock must be equal to the risk-free interest rate. The jump-diffusion model characterizes the default time/jump directly.

The jump-diffusion model was first introduced by Merton (1976) in the market risk context for modeling asset price behavior that incorporates small day-to-day diffusive movements together with larger randomly occurring jumps. Over the last decade, people attempt to propagate the model from the market risk domain to the credit risk arena. 

There are two primary types of models that attempt to describe default processes in the literature: structural models and reduced-form models. The structural models regard default as an endogenous event, focusing on the capital structure of a firm. The reduced-form models do not explain the event of a default endogenously, but instead characterize it exogenously as a jump process. Many practitioners in the credit trading arena have tended to gravitate toward the reduced-from models given their mathematical tractability and market consistency.

Zhou (1997), Hilberink and Rogers (2002), Chen and Kou (2009), etc. introduce the jump-diffusion mechanism into the structural models, while Davis and Lischka (1999), Andersen and Buffum (2004), and Bloomberg (2009), etc. add a default jump to the stock price dynamics. We refer to the formers as the structural jump-diffusion models and the latters as the reduced-form jump-diffusion models.

Although both the structural jump-diffusion model and the reduced-form model contain jumps, these jumps have different meanings: A jump in the structural jump-diffusion model corresponds to a sudden change in the asset value that may or may not cause the firm to default, whereas a jump in the reduced-form model represents the default event itself.

In this paper, we mainly discuss the reduced-form jump-diffusion models. At the heart of the jump-diffusion models lies the assumption that the total expected rate of return to the stockholders is equal to the risk-free interest rate under a risk-neutral measure.

Although we agree that under a risk-neutral measure the market price of risk and risk preferences are irrelevant to asset pricing (see Hull (2003)) and thereby the expectation of a risk-free  asset grows at the risk-free interest rate, we are not convinced that the expected rate of return on a defaultable asset must be also equal to the risk-free rate. We argue that unlike market risk, credit risk actually has a significant impact on asset prices. This is why regulators, such as International Accounting Standards Board (IASB), Basel Committee on Banking Supervision (BCBS), etc. require financial institutions to report a credit value adjustment (CVA) in addition to the risk-free mark-to-market (MTM) value to reflect credit risk (see Xiao (2013)). By definition, a CVA is the difference between the risk-free value and the risky value of an asset/portfolio subject to credit risk. CVA implies that the risk-free value should not be equal to the risky value in the presence of default risk. As a matter of fact, we will prove that the expected return of a defaultable asset under a risk-neutral measure actually grows at a risky rate rather than the risk-free rate. This conclusion is very important for risky valuation.

Because of their hybrid nature, convertible bonds attract different type of investors. Especially, convertible arbitrage hedge funds play a dominant role in primary issues of convertible debt. In fact, it is believed that hedge funds purchase 70% to 80% of the convertible debt offered in primary markets. A prevailing belief in the market is that convertible arbitrage is mainly due to convertible underpricing (i.e., the model prices are on average higher than the observed trading prices) (see Ammann, et al (2003), Choi, et al. (2009), Loncarski, et al. (2009), etc.). However, Agarwal, et al. (2007) and Batta, et al. (2007) argue that the excess returns from convertible arbitrage strategies are not mainly due to underpricing, but rather partly due to illiquid. Calamos (2011) believes that arbitrageurs in general take advantage of volatility. A higher volatility in the underlying equity translates into a higher value of the equity option and a lower conversion premium. Multiple views reveal the complexity of convertible arbitrage, involving taking positions in the convertible bond and the underlying asset that hedges certain risks but leaves managers exposed to other risks for which they reap a reward. 

This article makes a theoretical and empirical contribution to the study of convertible bonds. In contrast to the above mentioned literature, we present a model that is based on the probability distribution (or intensity) of a default jump (or a default time) rather than the default jump itself, as the default jump is usually inaccessible (see Duffie and Huang (1996), Jarrow and Protter (2004), etc). 

We model both equities and bonds as defaultable in a consistent way. When a firm goes bankrupt, the investors who take the least risk are paid first. Secured creditors have the best chances of seeing the value of their initial investments come back to them. Bondholders have a greater potential for recovering some their losses than stockholders who are last in line to be repaid and usually receive little, if anything. The default proceedings provide a justification for our modeling assumptions: Different classes of securities issued by the same company have the same default probability but different recovery rates. Given this model, we are able to back out the market prices.

Valuation under our risky model can be solved by common numerical methods, such as, Monte Carlo simulation, tree/lattice approaches, or partial differential equation (PDE) solutions. The PDE algorithm is elaborated in this paper, but of course the methodology can be easily extended to tree/lattice or Monte Carlo.

Using the model proposed, we conduct an empirical study of convertible bonds. We obtain a data set from FinPricing (2015). The data set contains 164 convertible bonds and 2 years of daily market prices as well as associated interest rate curves, credit curves, stock prices, implied Black-Scholes volatilities and recovery rates.

The most important input parameter to be determined is the volatility for valuation. A common approach in the market is to use the at-the-money (ATM) implied Black-Scholes volatility to price convertible bonds. However, most liquid stock options have relatively short maturates (rarely more than 8 years). As a result, some authors, such as Ammann, et al. (2003), Loncarski, et al. (2009), Zabolotnyuk, et al. (2010), have to make do with historical volatilities. Therefore, we segment the sample into two sets according to the time to maturity: a short-maturity class (0 ~ 8 years) and a long-maturity class (> 8 years). For the short-maturity class, we use the ATM implied Black-Scholes volatilities for valuation, whereas for the long-maturity class, we calculate the historical volatility as the annualized standard deviation of the daily log returns of the last 2 years and then price the convertible bond based on this real-world volatility.

The empirical results show that the model prices fluctuate randomly around the market prices, indicating the model is quite accurate. Our empirical evidence does not support a systematic underpricing hypothesis. A similar conclusion is reached by Ammann and Wilde (2008) who use a Monte-Carlo simulation approach. Moreover, market participants almost always calibrate their models to the observed market prices using implied convertible volatilities. Therefore, underpricing may not be the main driver of profitability in convertible arbitrage. 

It is useful to examine the basics of the convertible arbitrage strategy. A typical convertible bond arbitrage employs delta-neutral hedging, in which an arbitrageur buys a convertible bond and sells the underlying equity at the current delta (see Choi, et al. (2009), Loncarski, et al. (2009), etc.). With delta neutral positions, the sign of Gamma is important. If Gamma is negative, the portfolio profits so long as the underlying equity remains stable. If Gamma is positive, the portfolio will profit from large movements in the stock price in either direction (see Somanath (2011)).

We study the sensitivities of convertible bonds and find that convertible bonds have relatively large positive gammas, implying that convertible arbitrage can make a profit on a large upside or downside movement in the underlying stock price. Since convertible bonds are issued mainly by start-up or small companies (while more established firms rely on other means of financing), the chance of a large movement in either direction is very likely. Even for very small movements in the underlying stock price, profits can still be generated from the yield of the convertible bond and the interest rebate for the short position.

References

Agarwal, V., W. Fung, Y. Loon and N. Naik, (2007), “Liquidity provision in the convertible bond market: analysis of convertible arbitrage hedge funds,” CFR-working paper, Georgia State University.

Ammann, M., Kind, A., and Wilde, C., (2003), “Are convertible bonds underpriced? An analysis of the French market,” Journal of Banking & Finance Vol. 27, No. 4, pp. 635-653.

Ammann, M, Kind, A., and Wilde, C., (2008), “Simulation-based pricing of convertible bonds,” Journal of empirical finance, Vol. 15, No. 2, pp. 310-331.

Andersen, L. and Buffum, D., (2004), “Calibration and implementation of convertible bond models,” Journal of Computational Finance, Vol. 7, No. 4, pp. 1-34.

Ayache, E., Forsyth, P. A., and Vetzal, K. R., (2003), “The valuation of convertible bonds with credit risk,” Journal of Derivatives, Vol. 11, No. 1, pp. 9-30.

Batta, George, George Chacko, and Bala G. Dharan, (2010), “Valuation consequences of convertible debt issuance,” Journal of Fixed Income, Vol. 20, No. 1, pp. 28-43.

Bloomberg, (2009), “OVCV model description,” Quantitative research and development, Equities team.

Brennan, M. and Schwartz, E., (1980), “Analyzing convertible bonds,” Journal of Financial and Quantitative Analysis, Vol. 15, No. 4, pp. 907-929.

Calamos, Nick P., (2011), Convertible arbitrage: Insights and techniques for successful hedging, John Wiley & Sons.

Carr, P. and Linetsky, V., (2006), “A jump to default extended CEV model: an application of Bessel processes,” Finance and Stochastics, Vol. 10, No. 3, pp. 303-330.

Carayannopoulos, P. and Kalimipalli, M., (2003), “Convertible bond prices and inherent biases,” Journal of Fixed Income, Vol. 13, No. 3, pp. 64-73.

Chen, N. and S. Kou, (2009), “Credit spreads, optimal capital structure, and implied volatility with endogenous default and jump risk,” Mathematical Finance, Vol. 19, No. 2, pp. 343-378.

Choi, D., Getmansky, M. and Tookes, H., (2009), “Convertible bond arbitrage, liquidity externalities, and stock prices,” Journal of Financial Economics, Vol. 91, No. 2, 227-251.

Cox, J., S. Ross, and M. Rubinstein, (1979), "Option Pricing: A Simplified Approach." Journal of Financial Economics, Vol. 7, No. 3, pp. 229-263.

Davis, M. and Lischka, F. R., (1999), “Convertible bonds with market risk and credit risk,” Working paper, Tokyo-Mitsubishi International plc.

Duffie, D., and Huang, M., (1996), “Swap rates and credit quality,” Journal of Finance, Vol. 51, No. 3, pp. 921-949.

Duffie, D., and Singleton, K. J., (1999), “Modeling term structure of defaultable bonds,” Review of Financial Studies, Vol. 12, No. 4, pp. 687-720.

Ederington, L. and H. Lee, (1993), “How markets process information: News releases and volatility,” Journal of Finance, Vol. 48, No. 4, pp. 1161-1191.

FinPricing, 2015, Pricing Data, https://finpricing.com/lib/EqSwap.html

Grimwood, R., and Hodges, S., (2002), The valuation of convertible bonds: a study of alternative pricing models, Working paper, Warwick University.

Goldman Sachs, (1994), “Valuing convertible bonds as derivatives,” Quantitative Strategies Research Notes, Goldman Sachs.

Hilberink, B. and L. Rogers, (2002), “Optimal capital structure and endogenous default,” Finance Stochastics, Vol. 6, No. 2, pp. 237-263.

Hull, J., (2003), Options, Futures and Other Derivatives. 5th ed. Prentice Hall, Upper Saddle River, NJ.

Jarrow, R. A., and Protter, P., (2004), “Structural versus reduced form models: a new information based perspective,” Journal of Investment Management, Vol. 2, No. 2, pp. 34-43.

Loncarski, I., Horst, J., and Veld C., (2009), “The rise and demise of the convertible arbitrage strategy,” Financial Analysts Journal, Vol. 65, No. 5, pp. 35-50.

Merton, R. C., (1976), “Option pricing when underlying stock returns are discontinuous,” Journal of Financial Economy, Vol. 3, No. 1-2, pp. 125-144.

J. P. Morgan, (1999), “The J. P. Morgan guide to credit derivatives,” Risk Publications.

J.P. Morgan, (2001), “Par credit default swap spread approximation from default probabilities,” Risk Publications.

Somanath, V.S., (2011), International financial management, I.K. International Publishing House Pvt. Ltd.

Tsiveriotis, K. and Fernandes, C., (1998), “Valuing convertible bonds with credit risk,” Journal of Fixed Income, Vol. 8, No. 4, pp. 95–102.

Xiao, T., (2013), “An accurate solution for credit value adjustment (cva) and wrong way risk,” Working paper, SSRN.

Zabolotnyuk, Y., Jones, R., and Veld, C., (2010), “An empirical comparison of convertible bond valuation models,” Financial Management, Vol. 39, No. 2, pp. 675-705.

Zhou, C., (1997), “A jump-diffusion approach to modeling credit risk and valuing defaultable securities,” Technical Report, Federal Research Board, Washington DC.
